package com.example.sakura.pals.Models;

/**
 * Created by Liz Antoinette on 10/3/2017.
 */

public class Landlord {
    private boolean isRenting;
    private int landlordID;
    private int rating;
    private int userID;

    public Landlord(boolean isRenting, int landlordID, int rating, int userID) {
        this.isRenting = isRenting;
        this.landlordID = landlordID;
        this.rating = rating;
        this.userID = userID;
    }

    public boolean isRenting() {
        return isRenting;
    }

    public void setRenting(boolean renting) {
        isRenting = renting;
    }

    public int getLandlordID() {
        return landlordID;
    }

    public void setLandlordID(int landlordID) {
        this.landlordID = landlordID;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }
}
