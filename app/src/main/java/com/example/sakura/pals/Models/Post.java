package com.example.sakura.pals.Models;

import java.util.jar.Attributes;

public class Post {
    public String id;
    public String userId;
    public String displayName;
    public String price;
    public String location;

    public Post(String id, String userId, String displayName, String price, String location) {
        this.id = id;
        this.userId = userId;
        this.displayName = displayName;
        this.price = price;
        this.location = location;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String  getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }
}
