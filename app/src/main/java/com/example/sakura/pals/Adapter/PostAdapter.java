package com.example.sakura.pals.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sakura.pals.Models.Post;
import com.example.sakura.pals.R;

import java.util.List;

/**
 * Created by Liz Antoinette on 10/4/2017.
 */

public class PostAdapter extends RecyclerView.Adapter<PostAdapter.ViewHolder>{

    private List<Post> mPost;
    private Context mContext;

    public PostAdapter(List<Post> mPost, Context mContext) {
        this.mPost = mPost;
        this.mContext = mContext;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.post,null, false));
    }

    public List<Post> getmPost() {
        return mPost;
    }

    public void setmPost(List<Post> mPost) {
        this.mPost = mPost;
    }

    private int getColor(int color){
        return ContextCompat.getColor(mContext,color);

    }


    @Override
    public void onBindViewHolder(PostAdapter.ViewHolder holder, int position) {
        Post post= mPost.get(position);

        holder.username.setText(post.getDisplayName());
        holder.address.setText(post.getLocation());
        holder.price.setText(post.getPrice());

    }

    @Override
    public int getItemCount() {
        return mPost.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView username;
        TextView address;
        TextView price;
        ImageView userAvatar;
        ImageView postPicture;

        public ViewHolder(View itemView) {
            super(itemView);

            username=(TextView) itemView.findViewById(R.id.text_name);
            userAvatar=(ImageView) itemView.findViewById(R.id.avatar);
            address= (TextView) itemView.findViewById(R.id.text_address);
            price=(TextView) itemView.findViewById(R.id.price);
            postPicture=(ImageView) itemView.findViewById(R.id.post_pic);

        }
    }
}
