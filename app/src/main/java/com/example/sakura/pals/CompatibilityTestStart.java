package com.example.sakura.pals;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class CompatibilityTestStart extends AppCompatActivity {

    String result = "";

    View view;

    Button analyze;

    RadioButton answer1;
    RadioButton answer1_2;
    RadioButton answer2;
    RadioButton answer2_2;
    RadioButton answer3;
    RadioButton answer3_2;
    RadioButton answer4;
    RadioButton answer4_2;
    RadioButton answer5;
    RadioButton answer5_2;
    RadioButton answer6;
    RadioButton answer6_2;
    RadioButton answer7;
    RadioButton answer7_2;
    RadioButton answer8;
    RadioButton answer8_2;
    RadioButton answer9;
    RadioButton answer9_2;
    RadioButton answer10;
    RadioButton answer10_2;

    @Override
    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_compatibility_test_start);

        view = findViewById(R.id.compatibility_layout);

        answer1 = view.findViewById(R.id.answer1);
        answer1_2 = view.findViewById(R.id.answer1_2);
        answer2 = view.findViewById(R.id.answer2);
        answer2_2 = view.findViewById(R.id.answer2_2);
        answer3 = view.findViewById(R.id.answer3);
        answer3_2 = view.findViewById(R.id.answer3_2);
        answer4 = view.findViewById(R.id.answer4);
        answer4_2 = view.findViewById(R.id.answer4_2);
        answer5 = view.findViewById(R.id.answer5);
        answer5_2 = view.findViewById(R.id.answer5_2);
        answer6 = view.findViewById(R.id.answer6);
        answer6_2 = view.findViewById(R.id.answer6_2);
        answer7 = view.findViewById(R.id.answer7);
        answer7_2 = view.findViewById(R.id.answer7_2);
        answer8 = view.findViewById(R.id.answer8);
        answer8_2 = view.findViewById(R.id.answer8_2);
        answer9 = view.findViewById(R.id.answer9);
        answer9_2 = view.findViewById(R.id.answer9_2);
        answer10 = view.findViewById(R.id.answer10);
        answer10_2 = view.findViewById(R.id.answer10_2);

        analyze = view.findViewById(R.id.button_analyze);

    }
    public void onClickAnalyze(){
        analyze.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                int ones=0;
                int twos=0;

                if(answer1.isChecked())
                    ones++;
                else if(answer1_2.isChecked())
                    twos++;
                if(answer2.isChecked())
                    ones++;
                else if(answer2_2.isChecked())
                    twos++;
                if(answer3.isChecked())
                    ones++;
                else if(answer3_2.isChecked())
                    twos++;
                if(answer4.isChecked())
                    ones++;
                else if(answer4_2.isChecked())
                    twos++;
                if(answer5.isChecked())
                    ones++;
                else if(answer5_2.isChecked())
                    twos++;
                if(answer6.isChecked())
                    twos++;
                else if(answer6_2.isChecked())
                    ones++;
                if(answer7.isChecked())
                    ones++;
                else if(answer7_2.isChecked())
                    twos++;
                if(answer8.isChecked())
                    twos++;
                else if(answer8_2.isChecked())
                    ones++;
                if(answer9.isChecked())
                    twos++;
                else if(answer9_2.isChecked())
                    ones++;
                else if(answer10.isChecked())
                    twos++;
                else if(answer10_2.isChecked())
                    ones++;

                if(ones>6 &&twos<4)
                    result = "Extrovert";
                else if(twos>6 &&ones<4)
                    result = "Introvert";
                else
                    result = "Ambivert";

                Toast.makeText(CompatibilityTestStart.this, result, Toast.LENGTH_LONG).show();
                startActivity(new Intent(CompatibilityTestStart.this, Home.class));
            }
        });
    }

}
