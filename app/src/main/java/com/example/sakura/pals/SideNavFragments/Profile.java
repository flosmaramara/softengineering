package com.example.sakura.pals.SideNavFragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sakura.pals.Adapter.PagerAdapter;
import com.example.sakura.pals.Models.Post;
import com.example.sakura.pals.Adapter.PostAdapter;
import com.example.sakura.pals.R;
import com.google.firebase.auth.FirebaseAuth;

import java.util.ArrayList;
import java.util.List;

import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class Profile extends Fragment {
    private RecyclerView mRecyclerView;

    private List<Post> mAllPost;
    private PostAdapter mAdapter;
    private FirebaseAuth mAuth;
    private TextView user_name;
    private TextView about_user_name;
    private TextView about_user_number;
    private TextView about_user_email;

    public Profile() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Context context=inflater.getContext();
        View view= inflater.inflate(R.layout.fragment_profile, container, false);

        mAuth = FirebaseAuth.getInstance();

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs);
        tabLayout.addTab(tabLayout.newTab().setText("Post"));
        tabLayout.addTab(tabLayout.newTab().setText("Personality"));
        tabLayout.addTab(tabLayout.newTab().setText("About"));
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);

        user_name = (TextView)view.findViewById(R.id.user_name);
        user_name.setText(mAuth.getCurrentUser().getDisplayName());

        final ViewPager viewPager = (ViewPager) view.findViewById(R.id.pagers);
        final PagerAdapter adapter = new PagerAdapter
                (((AppCompatActivity) getActivity()).getSupportFragmentManager(), tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mAuth = FirebaseAuth.getInstance();

                viewPager.setCurrentItem(tab.getPosition());

                if(tab.getPosition() == 2){
                    about_user_name = (TextView)viewPager.findViewById(R.id.about_user_name);
                    about_user_number = (TextView)viewPager.findViewById(R.id.about_user_number);
                    about_user_email = (TextView)viewPager.findViewById(R.id.about_user_email);

                    about_user_name.setText(mAuth.getCurrentUser().getDisplayName());
                    about_user_number.setText(mAuth.getCurrentUser().getProviderId());
                    about_user_email.setText(mAuth.getCurrentUser().getEmail());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return view;
    }



}
