package com.example.sakura.pals.Models;

/**
 * Created by Liz Antoinette on 10/3/2017.
 */

public class Room {
    private Address address;
    private String roomType;
    private String status;
    private String roomName;
    private String features;
    private String roomFee;
    private Boolean isOccupied;
    private int roomOwnerID;
    private Date dateListed;
    private int roomID;

    public Room(Address address, String roomType, String status, String roomName, String features, String roomFee, Boolean isOccupied, int roomOwnerID, Date dateListed, int roomID) {
        this.address = address;
        this.roomType = roomType;
        this.status = status;
        this.roomName = roomName;
        this.features = features;
        this.roomFee = roomFee;
        this.isOccupied = isOccupied;
        this.roomOwnerID = roomOwnerID;
        this.dateListed = dateListed;
        this.roomID = roomID;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getFeatures() {
        return features;
    }

    public void setFeatures(String features) {
        this.features = features;
    }

    public String getRoomFee() {
        return roomFee;
    }

    public void setRoomFee(String roomFee) {
        this.roomFee = roomFee;
    }

    public Boolean getOccupied() {
        return isOccupied;
    }

    public void setOccupied(Boolean occupied) {
        isOccupied = occupied;
    }

    public int getRoomOwnerID() {
        return roomOwnerID;
    }

    public void setRoomOwnerID(int roomOwnerID) {
        this.roomOwnerID = roomOwnerID;
    }

    public Date getDateListed() {
        return dateListed;
    }

    public void setDateListed(Date dateListed) {
        this.dateListed = dateListed;
    }

    public int getRoomID() {
        return roomID;
    }

    public void setRoomID(int roomID) {
        this.roomID = roomID;
    }
}
