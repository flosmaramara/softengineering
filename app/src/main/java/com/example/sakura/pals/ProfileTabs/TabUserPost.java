package com.example.sakura.pals.ProfileTabs;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sakura.pals.Adapter.PostAdapter;
import com.example.sakura.pals.Models.Post;
import com.example.sakura.pals.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class TabUserPost extends Fragment {

    private RecyclerView mRecyclerView;

    private List<Post> mAllPost;
    private PostAdapter mAdapter;
    public TabUserPost() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Context context=inflater.getContext();
        View view= inflater.inflate(R.layout.fragment_tab_user_post, container, false);
        mRecyclerView= (RecyclerView) view.findViewById(R.id.list);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.VERTICAL,false));
        mRecyclerView.setAdapter(mAdapter=new PostAdapter(mAllPost=getPosts(), context ));
        return view;
    }
    private List<Post> getPosts(){
        return new ArrayList<Post>(){{
            add(new Post("Carol Bell", "Brgy. Walang Forever, Pangit St., Cebu City","http://kingofwallpapers.com/girl/girl-011.jpg","","P2,000"));
            add(new Post("Rochelle Yingst","Lovely Street, Cebu City","http://www.viraldoza.com/wp-content/uploads/2014/03/8876509-lily-pretty-girl.jpg","","P5,000"));
        }


        };
    }
}
