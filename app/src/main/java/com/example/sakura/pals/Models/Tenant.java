package com.example.sakura.pals.Models;

/**
 * Created by Liz Antoinette on 10/3/2017.
 */

public class Tenant {
    private boolean isSearching;
    private double budget;
    private Personality personality;
    private boolean hasRoom;
    private int userID;
    private int tenantID;

    public Tenant(boolean isSearching, double budget, Personality personality, boolean hasRoom, int userID, int tenantID) {
        this.isSearching = isSearching;
        this.budget = budget;
        this.personality = personality;
        this.hasRoom = hasRoom;
        this.userID = userID;
        this.tenantID = tenantID;
    }

    public boolean isSearching() {
        return isSearching;
    }

    public void setSearching(boolean searching) {
        isSearching = searching;
    }

    public double getBudget() {
        return budget;
    }

    public void setBudget(double budget) {
        this.budget = budget;
    }

    public Personality getPersonality() {
        return personality;
    }

    public void setPersonality(Personality personality) {
        this.personality = personality;
    }

    public boolean isHasRoom() {
        return hasRoom;
    }

    public void setHasRoom(boolean hasRoom) {
        this.hasRoom = hasRoom;
    }

    public int getUserID() {
        return userID;
    }

    public void setUserID(int userID) {
        this.userID = userID;
    }

    public int getTenantID() {
        return tenantID;
    }

    public void setTenantID(int tenantID) {
        this.tenantID = tenantID;
    }
}

