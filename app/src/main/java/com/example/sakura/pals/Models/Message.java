package com.example.sakura.pals.Models;

/**
 * Created by Liz Antoinette on 10/3/2017.
 */

public class Message {
    private String recipient;
    private int recipientID;
    private String sender;
    private int senderID;
    private String message;

    public Message(String recipient, int recipientID, String sender, int senderID, String message) {
        this.recipient = recipient;
        this.recipientID = recipientID;
        this.sender = sender;
        this.senderID = senderID;
        this.message = message;
    }

    public String getRecipient() {
        return recipient;
    }

    public void setRecipient(String recipient) {
        this.recipient = recipient;
    }

    public int getRecipientID() {
        return recipientID;
    }

    public void setRecipientID(int recipientID) {
        this.recipientID = recipientID;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public int getSenderID() {
        return senderID;
    }

    public void setSenderID(int senderID) {
        this.senderID = senderID;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
