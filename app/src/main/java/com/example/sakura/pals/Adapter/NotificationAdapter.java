package com.example.sakura.pals.Adapter;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sakura.pals.Models.Notification;
import com.example.sakura.pals.R;
import java.util.List;

/**
 * Created by Liz Antoinette on 10/4/2017.
 */

public class NotificationAdapter extends RecyclerView.Adapter<NotificationAdapter.ViewHolder> {

    private List<Notification> mNotification;
    private Context mContext;

    public NotificationAdapter(Context context, List<Notification> notifications){
        mContext=context;
        mNotification=notifications;
    }

    public List<Notification> getNotification() {
        return mNotification;
    }

    public void setNotification(List<Notification> mNotification) {
        this.mNotification = mNotification;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.notification_list,null,false));
    }

    @Override
    public void onBindViewHolder(NotificationAdapter.ViewHolder holder, int position) {
        Notification notification= mNotification.get(position);

        holder.notif_username.setText(notification.getUsername());
        holder.notif_date.setText(notification.getDate());
        holder.notif_description.setText(notification.getDescription());
        holder.notif_avatar_pic.setImageURI(Uri.parse(notification.getNotif_avatar_pic()));
    }
    class ViewHolder extends RecyclerView.ViewHolder{
        TextView notif_username;
        TextView notif_description;
        TextView notif_date;
        ImageView notif_avatar_pic;

        public ViewHolder(View itemView){
            super(itemView);
            notif_username=(TextView) itemView.findViewById(R.id.notif_avatar_name);
            notif_avatar_pic=(ImageView) itemView.findViewById(R.id.notif_avatar_pic);
            notif_description=(TextView) itemView.findViewById(R.id.notif_text);
            notif_date=(TextView) itemView.findViewById(R.id.notif_date);

        }


    }

    @Override
    public int getItemCount() {
        return mNotification.size();
    }
}
