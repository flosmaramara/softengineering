package com.example.sakura.pals.Adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.sakura.pals.ProfileTabs.TabUserAbout;
import com.example.sakura.pals.ProfileTabs.TabUserPersonality;
import com.example.sakura.pals.ProfileTabs.TabUserPost;

/**
 * Created by flos on 10/18/2017.
 */

public class PagerAdapter extends FragmentStatePagerAdapter {
    int mNumOfTabs;

    public PagerAdapter(FragmentManager fm, int NumOfTabs) {
        super(fm);
        setmNumOfTabs(NumOfTabs);
    }


    public void setmNumOfTabs(int mNumOfTabs) {
        this.mNumOfTabs = mNumOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                TabUserPost tab1 = new TabUserPost();
                return tab1;
            case 1:
                TabUserPersonality tab2 = new TabUserPersonality();
                return tab2;
            case 2:
                TabUserAbout tab3 = new TabUserAbout();
                return tab3;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}