package com.example.sakura.pals.BottomNavFragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.sakura.pals.Models.Post;
import com.example.sakura.pals.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * A simple {@link Fragment} subclass.
 */
public class AddPostContent extends Fragment {

    DatabaseReference databasePosts;
    FirebaseAuth mAuth;

    EditText address_post;
    EditText price_post;
    Context context;
    public AddPostContent() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
       context=inflater.getContext();

        databasePosts = FirebaseDatabase.getInstance().getReference("posts");
        mAuth = FirebaseAuth.getInstance();

        View view = inflater.inflate(R.layout.fragment_add_post, container, false);
        Button add_post = (Button)view.findViewById(R.id.button2);
        address_post = (EditText)view.findViewById(R.id.get_post_address);
        price_post = (EditText)view.findViewById(R.id.get_post_price);

        add_post.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addPost();
            }
        });
        return view;


    }

    public void addPost(){
        String address = address_post.getText().toString().trim();
        String price = price_post.getText().toString().trim();

        if(!TextUtils.isEmpty(address) && !TextUtils.isEmpty((price))){
            String id = databasePosts.push().getKey();
            String userId = mAuth.getCurrentUser().getUid();
            String postDisplayName = mAuth.getCurrentUser().getDisplayName().toString();

            Post posts = new Post(id,userId,postDisplayName,price,address);
            databasePosts.child(id).setValue(posts);

            Toast.makeText(context, "Post Added", Toast.LENGTH_LONG).show();

            address_post.setText("");
            price_post.setText("");
        }
    }

}
