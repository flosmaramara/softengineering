package com.example.sakura.pals.Models;

/**
 * Created by Liz Antoinette on 10/4/2017.
 */

public class Notification {
    private String username;
    private String description;
    private String Date;
    private String notif_avatar_pic;

    public Notification(String username, String description, String date, String notif_avatar_pic) {
        this.username = username;
        this.description = description;
        Date = date;
        this.notif_avatar_pic = notif_avatar_pic;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getNotif_avatar_pic() {
        return notif_avatar_pic;
    }

    public void setNotif_avatar_pic(String notif_avatar_pic) {
        this.notif_avatar_pic = notif_avatar_pic;
    }
}
