package com.example.sakura.pals;

import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.example.sakura.pals.BottomNavFragments.AddPostContent;
import com.example.sakura.pals.BottomNavFragments.HomeContent;
import com.example.sakura.pals.BottomNavFragments.MessagesContent;
import com.example.sakura.pals.BottomNavFragments.NotificationContent;
import com.example.sakura.pals.BottomNavFragments.SearchContent;
import com.example.sakura.pals.SideNavFragments.BookMarked;
import com.example.sakura.pals.SideNavFragments.CompatibilityTestContent;
import com.example.sakura.pals.SideNavFragments.Profile;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.auth.FirebaseAuth;

public class Home extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

        FirebaseAuth mAuth;
        TextView nav_user_name;

        GoogleApiClient mGoogleApiClient;


    @Override
    protected void onStart() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();
        mGoogleApiClient.connect();
        super.onStart();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        android.app.FragmentManager manager=getFragmentManager();
        HomeContent homeContent =new HomeContent();
        manager.beginTransaction().replace(R.id.content,homeContent, homeContent.getTag()).commit();

        mAuth = FirebaseAuth.getInstance();

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        View header = navigationView.getHeaderView(0);

        nav_user_name = (TextView)header.findViewById(R.id.nav_user_name);
        nav_user_name.setText(mAuth.getCurrentUser().getDisplayName());

        navigationView.setNavigationItemSelectedListener(this);



        BottomNavigationView navigation = (BottomNavigationView) findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);

    }

    private void signOut(){
       if(mGoogleApiClient.isConnected()){
           Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                   new ResultCallback<Status>() {
                       @Override
                       public void onResult(@NonNull Status status) {
                           mAuth.signOut();
                           Toast.makeText(getApplicationContext(), "Signing out", Toast.LENGTH_SHORT).show();
                           startActivity(new Intent(getApplicationContext(), LogIn.class));
                       }
                   }
           );
       }
    }


    private BottomNavigationView.OnNavigationItemSelectedListener mOnNavigationItemSelectedListener
            = new BottomNavigationView.OnNavigationItemSelectedListener() {

        @Override
        public boolean onNavigationItemSelected(@NonNull MenuItem item) {
            android.app.FragmentManager manager=getFragmentManager();

            switch (item.getItemId()) {
                case R.id.navigation_home:
                    HomeContent homeContent= new HomeContent();
                    manager.beginTransaction().replace(R.id.content,
                            homeContent,
                            homeContent.getTag()).commit();
                    setTitle("Home");
                    return true;
                case R.id.navigation_search:
                    SearchContent search= new SearchContent();
                    manager.beginTransaction().replace(R.id.content,
                            search,
                            search.getTag()).commit();
                    setTitle("Search");
                    return true;
                case R.id.navigation_addPost:
                    AddPostContent addPost = new AddPostContent();
                    manager.beginTransaction().replace(R.id.content,
                            addPost,
                            addPost.getTag()).commit();
                    setTitle("Post");
                    return true;
                case R.id.navigation_messages:
                    MessagesContent messages= new MessagesContent();
                    manager.beginTransaction().replace(R.id.content,
                            messages,
                            messages.getTag()).commit();
                    setTitle("Messages");

                    return true;
                case R.id.navigation_notification:
                    NotificationContent notification= new NotificationContent();
                    manager.beginTransaction().replace(R.id.content,
                            notification,
                            notification.getTag()).commit();
                    setTitle("Notifications");
                    return true;
            }
            return false;
        }

    };



    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        FragmentManager manager=getFragmentManager();
        if (id == R.id.nav_profile) {
           manager.beginTransaction()
                   .replace(R.id.content,
                           new Profile())
                   .commit();
            setTitle("Profile");
        } else if(id==R.id.nav_test){
            manager.beginTransaction()
                    .replace(R.id.content,
                            new CompatibilityTestContent())
                    .commit();

            setTitle("Personality Test");
        }else if (id == R.id.nav_bookmark) {
            manager.beginTransaction()
                    .replace(R.id.content,
                            new BookMarked())
                    .commit();
            setTitle("Bookmarks");
        } else if (id == R.id.nav_logout) {
            signOut();
            //Toast.makeText(this, mAuth.getCurrentUser().getEmail().toString(), Toast.LENGTH_LONG).show();
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void goToProfile(View view){
        android.app.FragmentManager manager=getFragmentManager();

        Profile profile= new Profile();
        manager.beginTransaction().replace(R.id.content,
                profile,
                profile.getTag()).commit();
    }
}
