package com.example.sakura.pals.BottomNavFragments;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.sakura.pals.Models.Notification;
import com.example.sakura.pals.Adapter.NotificationAdapter;
import com.example.sakura.pals.R;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class NotificationContent extends Fragment {
    private RecyclerView mRecyclerView;
    private List<Notification> mAllNotification;
    public NotificationAdapter mAdapter;

    public NotificationContent() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Context context= inflater.getContext();
        View view= inflater.inflate(R.layout.fragment_notification, container, false);
        mRecyclerView=(RecyclerView) view.findViewById(R.id.list_notification);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(context,LinearLayoutManager.VERTICAL,false));
        mRecyclerView.setAdapter(mAdapter= new NotificationAdapter(context,mAllNotification=getAllNotification()));
        return view;
    }

    private List<Notification> getAllNotification(){
        return new ArrayList<Notification>(){{
            add(new Notification("Liz Antoinette Anguren","Liked your post","2 min ago",""));
            add(new Notification("Talia Mihanya", "Added a new post", "1 hr ago", ""));
        }

        };
    }

}
